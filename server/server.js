var express = require("express");
var path = require("path");
var http = require("http");
var usersApi = require('./routes/users');
var invertersApi = require('./routes/inverters');

var app = express();
app.use( express.static(__dirname+'/../client'));

app.use(function(request, response, next) {
    console.log("In comes a request to: " + request.url);
    next();
});
app.use("/users", usersApi);
app.use("/inverters", invertersApi);


app.use(function(request, response) {
    response.status(404).render("404");
});
/**
 *  Have our server listen on port 3000
 */
var port = process.env.PORT || 3000;
http.createServer(app).listen(port, function(){
    console.log('Please open http://localhost:%d/', port);
});