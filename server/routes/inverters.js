var express = require("express");
var router = express.Router();
var auth_token = require('../token');


router.get("/", function (request, response, next) {
    let token = request.headers.token;
    if (token === auth_token) {
        function randomNumber(minimum, maximum){
            return parseFloat(Math.min(minimum + (Math.random() * (maximum - minimum)),maximum).toFixed(2));

        }
        response.json({
            'pdc': randomNumber(1000,2000),
            'pac' : randomNumber(1000,2000),
            'q': randomNumber(0,10),
            'term': randomNumber(80,20),
            'dprod': randomNumber(50,150),
            'status': Math.floor(Math.random() +1) === 1 ?'Online':'Offline',
            'name': 'Conext SmartGen 1'
        });

    } else {
        response.send(401, 'Wrong userName or password');
    }
});
module.exports = router;