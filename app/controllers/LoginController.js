/**
 * Login class controller.
 */
export default class LoginController {
    constructor($scope, $state, dataService) {
        $scope.userName = '';
        $scope.password = '';
        $scope.errorMessage = '';
        $scope.refresh_time = null;
        /**
         * Remove error message form page
         */
        $scope.closeAlert = function () {
            $scope.errorMessage = '';
        };
        /**
         * Refresh server time.
         */
        $scope.refresh = function () {
            $scope.refresh_time = new Date();
        };
        /**
         * Submit form action.
         */
        $scope.tryLogin = function () {
            $scope.errorMessage = '';
            /**
             * User credentials validation.
             */
            if ($scope.userName === '') {
                $scope.errorMessage = 'User name can not be empty.';
            }
            if ($scope.password === '') {
                $scope.errorMessage = 'Password can not be empty.';
            }

            if ($scope.errorMessage === '') {
                /**
                 * Submit authentication request to the server API
                 */
                dataService.tryLogin($scope.userName, $scope.password).then(function (token) {
                        $state.go('inverter');
                    }, function (err) {
                        $scope.errorMessage = err;
                    }
                );
            }
        };
        $scope.refresh();
    }
}
/**
 *Inject dependency
 */
LoginController.$inject = ['$scope', '$state', 'dataService'];