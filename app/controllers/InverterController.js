/**
 * Inverter class controller.
 */
export default class InverterController {
    constructor($scope, dataService) {
        $scope.data = null;
        /**
         * Pull data form server every 3 sec.
         */
        function loadData() {
            dataService.getInverterData().then(function (data) {
                $scope.data = data;
                setTimeout(function () {
                    loadData();
                },3000);
            });
        }
        loadData();
    }
}
/**
 *Inject dependency
 */
InverterController.$inject = ['$scope', 'dataService'];
