import angular from 'angular'
import 'angular-ui-router'
import 'angular-ui-bootstrap'
import dataService from './service'
import LoginController from './controllers/LoginController'
import InverterController from './controllers/InverterController'

var webApp = angular.module('webApp', ['ui.router', 'ui.bootstrap']);

webApp.config(function ($stateProvider, $urlRouterProvider) {
    // default load login page
    $urlRouterProvider.otherwise('/login');
    // routing
    $stateProvider
        .state('login', {
            url: '/login',
            views: {
                'main-view': {
                    templateUrl: '/assets/html/login.html',
                    controller: 'LoginController'
                }
            }
        }).state('inverter', {
        url: '/inverter',
        views: {
            'main-view': {
                templateUrl: '/assets/html/inverter.html',
                controller: 'inverterController'
            }
        }
    });
})
    .controller('LoginController', LoginController)
    .controller('inverterController', InverterController)
    .service('dataService', dataService)
    .config(function ($httpProvider) {
        // Pull the dependency injector
        $httpProvider.interceptors.push(function ($injector, $q) {
            return {
                request: function (req) {
                    // Set the `token` header for every outgoing HTTP request
                    let dataService = $injector.get('dataService');

                    req.headers.token = dataService.getToken();
                    return req;
                },
                // This is the responseError interceptor
                responseError: function (rejection) {
                    if (rejection.status === 401) {
                        let $state = $injector.get('$state');
                        $state.go('login');
                    }

                    /* If not a 401, do nothing with this error.
                     * This is necessary to make a `responseError`
                     * interceptor a no-op. */
                    return $q.reject(rejection);
                }

            }
        })
    });
