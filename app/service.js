/**
 * DataService class.
 */
export default class dataService {

    /**
     * Data service constructor.
     *
     * @param $http   HTTP angular service
     * @param $state  State angular service
     * @param $q      Q angular service
     */
    constructor($http, $state, $q) {
        this.$http = $http;
        this.$q = $q;
        this.$state = $state;
        this.auth_token = null;
    }

    /**
     * Get current token.
     *
     * @returns {String}
     */
    getToken() {
        return this.auth_token;
    }

    /**
     * Authorization request.
     *
     * @returns {Promise}
     */
    tryLogin(userName, password) {
        let defer = this.$q.defer();

        this.$http({method: 'GET', url: `/users/${userName}/${password}`}).then((response)=> {
            if (response.data.token) {
                this.auth_token = response.data.token;
                defer.resolve(this.auth_token);
            }
        }, (err) => {
            defer.reject(err.data);
        });

        return defer.promise;

    }

    /**
     * Get inverter data form server.
     *
     * @returns {Promise}
     */
    getInverterData() {
        let defer = this.$q.defer();
        this.$http({method: 'GET', url: '/inverters'}).then((response)=> {
            defer.resolve(response.data);
        }, (err) => {
            defer.reject(err);
        });
        return defer.promise;

    }
}
/**
 *Inject dependency
 */
dataService.$inject = ['$http', '$state', '$q'];