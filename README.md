# Schneider Electric - Technical Test
  
Pure Javascript Object Oriented Application with ES2015 features 

## Please note:

Login credentials:
 
 UserName: *test* 
 
 Password: *test1234*



## Getting started

To run this application, you'll need:

1. [Node.js](http://nodejs.org) installed

## Running Schneider Electric - Technical Test

### 1.Install project dependency library

*To transpile ES2015 modules into regular es5 Javascript and bundle them into single file, I used :*  
* babelify
* browserify
* watchify 

*To build this application I used :*
* angular
* angular-ui-router
* angular-ui-bootstrap 


*To run this application local, and for Rest API I used:*
     
* express 


```sh

cd <project folder>

npm install

```
  
 ### 2.Compile javaScript code form es6 code into es5
```sh

cd <project folder>

npm start

```

### 3. Run Application on local server

```sh

cd <project folder>

node server/server.js

```

## Design Patterns

1. I used **modules** form ES2015 which allow use to isolate code into separate files.
2. **Classes** as well allow isolate and have ability easy extend the code.
3. **Service** allow having access into API in any place of this application.
4. For Application  and  Service,  I used **singleton** pattern.
5. I used  **Promise** for async work with API and HTML generation.
 
 
## Additional information
Instead of images icon I prefer to use fonts icons, so I converted all icons into fonts. There was an option to update inverter via web-socket, but it would require additional backend logic. But as it is only test task, I chose more simple way(REST API) to avoid any complications.